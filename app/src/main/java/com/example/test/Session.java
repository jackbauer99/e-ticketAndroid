package com.example.test;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {

    public static final String SESSION_APPS = "pdam";

    public static final String SESSION_PETUGAS_ID = "SESSION_PETUGAS_ID";
    public static final String SESSION_PETUGAS_FOTO = "SESSION_PETUGAS_FOTO";
    public static final String SESSION_PETUGAS_NAMA = "SESSION_PETUGAS_NAMA";
    public static final String SESSION_PETUGAS_NIP = "SESSION_PETUGAS_NIP";
    public static final String SESSION_PETUGAS_ROLE = "SESSION_PETUGAS_ROLE";
    public static final String SESSION_TOKEN_API = "SESSION_TOKEN_API";

    public static final String SESSION_LOGIN = "pdam_login";

    SharedPreferences Session;
    SharedPreferences.Editor SessionEditor;

    public Session(Context context) {
        Session = context.getSharedPreferences(SESSION_APPS, Context.MODE_PRIVATE);
        SessionEditor = Session.edit();
    }

    public void saveSessionString(String keySession, String value) {
        SessionEditor.putString(keySession,value);
        SessionEditor.commit();
    }

    public void saveSessionBool(String keySession, boolean value) {
        SessionEditor.putBoolean(keySession,value);
        SessionEditor.commit();
    }

    public void ResetSession() {
        SessionEditor.clear();
        SessionEditor.commit();
    }

    public boolean isLogin() {
        return Session.getBoolean(SESSION_LOGIN, false);
    }

    public String getDataRead(String key) {
        return Session.getString(key,"");
    }
}
