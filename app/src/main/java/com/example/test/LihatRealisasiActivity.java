package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.test.network.BaseAPIService;
import com.example.test.network.ConnectorAPI;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.text.Html;

public class LihatRealisasiActivity extends AppCompatActivity implements View.OnClickListener{

    TextView keluhan,pelanggan_nama,pelanggan_alamat;
    EditText realisasi;
    String keluhan_nama, nama_pelanggan, alamat_pelanggan, id, token;
    ImageView thumb1,thumb2,thumb3,thumb4;

    Uri uri1,uri2,uri3,uri4;
    Context mContext ;

    private static final int PICK_IMAGE = 1;
    private static final int PERMISSION_REQUEST_STORAGE = 2;

    Button realisasi_foto_1,realisasi_foto_2,realisasi_foto_3,realisasi_foto_4,kirim_realisasi;

    ProgressDialog loading;
    BaseAPIService APIServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lihat_realisasi);
        initcomponents();
        Bundle extras = getIntent().getExtras();
        if(extras != null) {

            final String URL;
            keluhan_nama = extras.getString("result_keluhan");
            Spanned keluhan_text = Html.fromHtml(keluhan_nama);
            keluhan.setText(keluhan_text);
            nama_pelanggan = extras.getString("result_pelanggan");
            alamat_pelanggan = extras.getString("result_alamat");
            pelanggan_nama.setText("Nama Pelanggan : "+nama_pelanggan);
            pelanggan_alamat.setText("Alamat Pelanggan : "+alamat_pelanggan);
        }
        APIServices = ConnectorAPI.getAPIService();
    }

    private void initcomponents() {
        keluhan = (TextView)findViewById(R.id.keluhan);
        realisasi = (EditText) findViewById(R.id.realisasi_nama);
        pelanggan_nama = (TextView)findViewById(R.id.nama_pelanggan);
        pelanggan_alamat = (TextView)findViewById(R.id.alamat_pelanggan);
        kirim_realisasi = (Button)findViewById(R.id.kirim_realisasi);
        realisasi_foto_1 = (Button)findViewById(R.id.realisasi_foto_1);
        thumb1 = (ImageView)findViewById(R.id.img_thumb1);
        realisasi_foto_1.setOnClickListener(this);
        kirim_realisasi.setOnClickListener(this);
    }

    private void pilihFoto() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);

        }else{
            bukaGaleri();
        }
    }

    private void bukaGaleri() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), PICK_IMAGE);
    }

    @Override
    public void onClick(View view) {
        if(view == realisasi_foto_1) {
            pilihFoto();
        } else if(view == kirim_realisasi) {
            if(uri1 != null) {
                Bundle extras = getIntent().getExtras();
                id = extras.getString("result_id");
                token = extras.getString("token");
//              loading = ProgressDialog.show(mContext,null,"Tolong Tunggu Sebentar"
//                    ,true,false);
                File file1 = FileUtils.getFile(this, uri1);
                updateRealisasi(id,token,file1);
            } else {
                Toast.makeText(this, "Anda Harus Memilih Foto Dulu", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK && data != null) {
            uri1 = data.getData();
            thumb1.setImageURI(uri1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    bukaGaleri();
                }

                return;
            }
        }
    }

    private void updateRealisasi(String id, String token, File file1) {
        RequestBody realisasi_nama = RequestBody.create(MediaType.parse("multipart/form-data"),realisasi.getText().toString());

        RequestBody photoBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
        MultipartBody.Part photoPart1 = MultipartBody.Part.createFormData("realisasi_foto_1",
                file1.getName(), photoBody1);

        APIServices.realisasiUpload(id,token,realisasi_nama,photoPart1).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try{
                        JSONObject JSONresult = new JSONObject(response.body().string());
                        if(JSONresult.getString("status").equals("true")) {
                            String pesan_sukses = JSONresult.getString("messages").toString();
                            Toast.makeText(LihatRealisasiActivity.this ,pesan_sukses, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LihatRealisasiActivity.this,DashboardActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("debug","onFailure: ERROR > " + t.toString());
                loading.dismiss();
            }
        });
    }


}
