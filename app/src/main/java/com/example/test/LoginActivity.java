package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.test.network.BaseAPIService;
import com.example.test.network.ConnectorAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText user_username;
    EditText user_userpassword;
    Button login;
    ProgressDialog loading;
    Context mContext ;
    BaseAPIService ApiServices;
    String foto,nama_petugas,id,petugas_nip,token,role;

    Session Session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Session = new Session(this);
        if(Session.isLogin()) {
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
        setContentView(R.layout.activity_login1);
        mContext = this;
        ApiServices = ConnectorAPI.getAPIService();
        initComponents();
    }

    public void initComponents(){
        user_username = (EditText)findViewById(R.id.petugas_username);
        user_userpassword = (EditText)findViewById(R.id.password);
        login = (Button)findViewById(R.id.btnLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(mContext,null,"Tolong Tunggu Sebentar"
                        ,true,false);
                requestLogin();
            }
        });
    }

    private void requestLogin() {
        ApiServices.loginRequest(user_username.getText().toString(),
                user_userpassword.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    loading.dismiss();
                    try{
                        JSONObject auth = new JSONObject(response.body().string());
                        if(auth.getString("success").equals("true")){
                            String pesan_sukses = auth.getString("messages").toString();
                            Toast.makeText(mContext ,pesan_sukses, Toast.LENGTH_SHORT).show();
                            JSONObject petugas = new JSONObject(auth.getString("data").toString());
                            foto = petugas.getString("petugas_foto").toString();
                            nama_petugas = petugas.getString("petugas_nama").toString();
                            id = petugas.getString("id").toString();
                            role = petugas.getString("role_id").toString();
                            petugas_nip = petugas.getString("petugas_nip").toString();
                            token = auth.getString("token").toString();
                            Intent intent = new Intent(mContext,DashboardActivity.class);
                            Session.saveSessionString(Session.SESSION_TOKEN_API, token);
                            Session.saveSessionString(Session.SESSION_PETUGAS_FOTO, foto);
                            Session.saveSessionString(Session.SESSION_PETUGAS_NAMA, nama_petugas);
                            Session.saveSessionString(Session.SESSION_PETUGAS_ID, id);
                            Session.saveSessionString(Session.SESSION_PETUGAS_ROLE, role);
                            Session.saveSessionString(Session.SESSION_PETUGAS_NIP, petugas_nip);
                            Session.saveSessionBool(Session.SESSION_LOGIN, true);
                            startActivity(intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("debug","onFailure: ERROR > " + t.toString());
                loading.dismiss();
            }
        });
    }
}
