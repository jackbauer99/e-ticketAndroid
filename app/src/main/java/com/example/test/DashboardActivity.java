package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.test.network.BaseAPIService;
import com.example.test.network.ConnectorAPI;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {

    String foto,nama,nip,id,pesan,hitung,token,role,keluhan,id_realisasi,nama_pelanggan,alamat_pelanggan;
    CardView Tiket;
    ImageView img;
    TextView nama_petugas, nip_petugas;
    ProgressDialog loading;
    Context context;
    BaseAPIService ApiServices;
    Button logout;
    Session Session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Session = new Session(this);
        setContentView(R.layout.activity_dashboard);
        context = this;
        initComponents();
        final String URL;
        foto = Session.getDataRead("SESSION_PETUGAS_FOTO");
        URL = "http://eticketpdamsby.xyz/"+foto;
        Glide.with(this).load(URL).apply(RequestOptions.circleCropTransform()).into(img);
        nama = Session.getDataRead("SESSION_PETUGAS_NAMA");
        nip = Session.getDataRead("SESSION_PETUGAS_NIP");
        nama_petugas.setText(nama);
        nip_petugas.setText(nip);
        Tiket = (CardView)findViewById(R.id.ambilID);
    }

    public void initComponents() {
        img = (ImageView)findViewById(R.id.foto);
        nama_petugas = (TextView)findViewById(R.id.nama_petugas);
        nip_petugas = (TextView)findViewById(R.id.nip_petugas);
        logout = (Button)findViewById(R.id.button_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLogout();
            }
        });
    }

    public void Click(View view) {
        id = Session.getDataRead("SESSION_PETUGAS_ID");
        token = Session.getDataRead("SESSION_TOKEN_API");
        role = Session.getDataRead("SESSION_PETUGAS_ROLE");
        if(!role.equals("1")) {
            Intent intent = new Intent(context,AmbilTicket.class);
            intent.putExtra("result_id",id);
            intent.putExtra("token",token);
            startActivity(intent);
        } else {
            Toast.makeText(context,"Dilarang Akses Activity Ini",Toast.LENGTH_SHORT).show();
        }
    }

    public void Realisasi(View view) {
        ApiServices = ConnectorAPI.getAPIService();
        id = Session.getDataRead("SESSION_PETUGAS_ID");
        token = Session.getDataRead("SESSION_TOKEN_API");
        role = Session.getDataRead("SESSION_PETUGAS_ROLE");
        loading = ProgressDialog.show(context,null,"Harap Ditunggu",
                true,false);
        lihatrealiasasi(id,token);
    }

    private void requestLogout() {
        Session.ResetSession();
        Session.saveSessionBool(Session.SESSION_LOGIN, false);
        Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
        startActivity(intent
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }

    private void lihatrealiasasi(String id, String token) {
        ApiServices.realisasiRequest(id,token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    loading.dismiss();
                    try{
                        JSONObject JSONResult = new JSONObject(response.body().string());
                        if(JSONResult.getString("status").equals("true")){
                            hitung = JSONResult.getString("hitung").toString();
                            if(hitung.equals("1")) {
                                pesan = JSONResult.getString("messages").toString();
                                Toast.makeText(context, pesan, Toast.LENGTH_SHORT).show();
                                JSONArray realisasi = new JSONArray(JSONResult.getString("realisasi").toString());
                                for (int i=0; i<realisasi.length(); i++) {
                                    JSONObject keluhanobj = realisasi.getJSONObject(i);
                                    keluhan = keluhanobj.getString("keluhan").toString();
                                    id_realisasi = keluhanobj.getString("id").toString();
                                    nama_pelanggan = keluhanobj.getString("nama_pelanggan").toString();
                                    alamat_pelanggan = keluhanobj.getString("alamat_pelanggan").toString();
                                }
                                Intent intent = new Intent(context,LihatRealisasiActivity.class);
                                intent.putExtra("token",Session.getDataRead("SESSION_TOKEN_API"));
                                intent.putExtra("result_keluhan",keluhan);
                                intent.putExtra("result_id",id_realisasi);
                                intent.putExtra("result_pelanggan",nama_pelanggan);
                                intent.putExtra("result_alamat",alamat_pelanggan);
                                startActivity(intent);
                            }
                        } else if(JSONResult.getString("status").equals("false")) {
                            hitung = JSONResult.getString("hitung").toString();
                            if(hitung.equals("0")) {
                                String pesan_error = JSONResult.getString("messages").toString();
                                Toast.makeText(context, pesan_error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    loading.dismiss();
                    Toast.makeText(context, "Error Tidak Bisa Menampilkan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("debug","onFailure: ERROR > " + t.toString());
                loading.dismiss();
            }
        });
    }
}
